{
  "name": "Changeling",
  "type": "race",
  "img": "icons/magic/control/silhouette-hold-change-green.webp",
  "effects": [],
  "folder": "OUbhGLupK57uz5n2",
  "flags": {
    "betterRolls5e": {
      "quickDesc": {
        "value": true,
        "altValue": true
      },
      "quickProperties": {
        "value": true,
        "altValue": true
      },
      "quickOther": {
        "value": true,
        "altValue": true
      },
      "quickFlavor": {
        "value": true,
        "altValue": true
      }
    },
    "core": {},
    "mre-dnd5e": {
      "formulaGroups": [
        {
          "label": "Primary",
          "formulaSet": []
        }
      ]
    },
    "magicitems": {
      "enabled": false,
      "equipped": false,
      "attuned": false,
      "charges": "0",
      "chargeType": "c1",
      "destroy": false,
      "destroyFlavorText": "reaches 0 charges: it crumbles into ashes and is destroyed.",
      "rechargeable": false,
      "recharge": "0",
      "rechargeType": "t1",
      "rechargeUnit": "r1",
      "sorting": "l"
    },
    "exportSource": {
      "world": "test",
      "system": "dnd5e",
      "coreVersion": "11.315",
      "systemVersion": "3.0.4"
    }
  },
  "system": {
    "description": {
      "value": "<p><em>With ever-changing appearances, changelings reside in many societies undetected. Each changeling can supernaturally adopt any face they like. For some changelings, a new face is only a disguise. For other changelings, a new face may reveal an aspect of their soul.</em></p><p><em>The first changelings in the multiverse appeared in the Feywild, and the wondrous, mutable essence of that plane lingers in changelings today — even in those changelings who have never set foot in the fey realm. Each changeling decides how to use their shape-shifting ability, channeling either the peril or the joy of the Feywild. Sometimes they adopt new forms for the sake of mischief or malice, and other times they don a new identity to right wrongs or delight the downtrodden.</em></p><p><em>In their true form, changelings appear faded, their features almost devoid of detail. It is rare to see a changeling in that form, for a typical changeling changes their shape the way others might change clothes. A casual shape — one created on the spur of the moment, with no depth or history — is called a mask. A mask can be used to express a mood or to serve a specific purpose and then might never be used again. However, many changelings develop identities that have more depth, crafting whole personas complete with histories and beliefs. A changeling adventurer might have personas for many situations, including negotiation, investigation, and combat.</em></p><p><em>Personas can be shared by multiple changelings; a community might be home to three healer changelings, with whoever is on duty adopting the persona of Andrea, the gentle physician. Personas can even be passed down through a family, allowing a younger changeling to take advantage of contacts established by the persona’s previous users.</em></p><p><strong>Ability Score Increase.</strong> When determining your character’s ability scores, increase one score by 2 and increase a different score by 1, or increase three different scores by 1. You can't raise any of your scores above 20.</p><p><strong>Creature Type.</strong> You are a Fey.</p><p><strong>Size.</strong> You are Medium or Small. You choose the size when you select this race.</p><p><strong>Speed.</strong> Your walking speed is 30 feet.</p><p><strong>Changeling Instincts.</strong> Thanks to your connection to the fey realm, you gain proficiency with two of the following skills of your choice: Deception, Insight, Intimidation, Performance, or Persuasion.</p><p><strong>Shapechanger.</strong> As an action, you can change your appearance and your voice. You determine the specifics of the changes, including your coloration, hair length, and sex. You can also adjust your height and weight and can change your size between Medium and Small. You can make yourself appear as a member of another race, though none of your game statistics change. You can’t duplicate the appearance of an individual you’ve never seen, and you must adopt a form that has the same basic arrangement of limbs that you have. Your clothing and equipment aren’t changed by this trait.</p><ul><li>You stay in the new form until you use an action to revert to your true form or until you die.</li></ul><p><strong>Languages.</strong> Your character can speak, read, and write Common and one other language that you and your DM agree is appropriate for the character. The <em>Player’s Handbook</em> offers a list of languages to choose from. The DM is free to modify that list for a campaign.</p>",
      "chat": ""
    },
    "source": {
      "custom": "MotM",
      "book": "",
      "page": "",
      "license": ""
    },
    "type": {
      "value": "fey",
      "subtype": "",
      "custom": ""
    },
    "advancement": [
      {
        "_id": "tscuTdZVBbdBzLcU",
        "type": "Size",
        "configuration": {
          "sizes": [
            "sm",
            "med"
          ],
          "hint": ""
        },
        "level": 0,
        "title": "",
        "icon": null,
        "value": {}
      },
      {
        "_id": "7LJZP9MG2Zj1Rf69",
        "type": "AbilityScoreImprovement",
        "configuration": {
          "points": 3,
          "fixed": {
            "str": 0,
            "dex": 0,
            "con": 0,
            "int": 0,
            "wis": 0,
            "cha": 0
          },
          "cap": 2
        },
        "value": {
          "type": "asi"
        },
        "level": 0,
        "title": "",
        "icon": null
      },
      {
        "_id": "yvAWLmRXX1kB4QQW",
        "type": "Trait",
        "configuration": {
          "mode": "default",
          "allowReplacements": false,
          "grants": [
            "languages:standard:common"
          ],
          "choices": [
            {
              "count": 1,
              "pool": [
                "languages:*"
              ]
            }
          ],
          "hint": ""
        },
        "level": 0,
        "title": "",
        "icon": null,
        "value": {}
      },
      {
        "_id": "4yTON3R8ZupSBlbZ",
        "type": "Trait",
        "configuration": {
          "mode": "default",
          "allowReplacements": false,
          "grants": [],
          "choices": [
            {
              "count": 2,
              "pool": [
                "skills:dec",
                "skills:ins",
                "skills:itm",
                "skills:prf",
                "skills:per"
              ]
            }
          ],
          "hint": ""
        },
        "level": 0,
        "title": "",
        "icon": null,
        "value": {}
      },
      {
        "_id": "gC23yxSY9M9lTlKy",
        "type": "ItemGrant",
        "configuration": {
          "items": [
            "Item.0MFpmH694ll4zQHA"
          ],
          "optional": false,
          "spell": null
        },
        "value": {},
        "level": 0
      }
    ],
    "movement": {
      "burrow": null,
      "climb": null,
      "fly": null,
      "swim": null,
      "walk": 30,
      "units": null,
      "hover": false
    },
    "senses": {
      "darkvision": null,
      "blindsight": null,
      "tremorsense": null,
      "truesight": null,
      "units": null,
      "special": ""
    }
  },
  "_stats": {
    "systemId": "dnd5e",
    "systemVersion": "3.0.4",
    "coreVersion": "11.315",
    "createdTime": 1710522231898,
    "modifiedTime": 1711363510560,
    "lastModifiedBy": "x6dGNqF45R5Qh86k"
  }
}